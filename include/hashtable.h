/*******************************************************************************
 * hashtable.h
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_

/**
 * Simple Hashtable for 4 byte integers.
 */

#ifdef _cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

typedef struct _Hashtable Hashtable;

/**
 * Here size should be a prime number about 10% of the actual expected number
 * of entries. If size is not a prime number the hashtable will perform
 * inefficiently.
 */
Hashtable * hashtable_new(size_t size);

/**
 * Deletes a hashtable and all of it's entries.
 */
void hashtable_delete(Hashtable * ht, void (*callback)(void * obj, void * data), void * cb_data);

/**
 * Attempts to insert a new element into the hashtable. This operation returns
 * 0 on success -1 in case it fails due to memory allocation and -2 in case it
 * fails due to an equal pre-existing key.
 */
int hashtable_insert(Hashtable * ht, uint32_t key, void * data);

/**
 * Removes a key from the hashtable and returns it's value. This function
 * behaves mostly like hashtable_get().
 */
void * hashtable_remove(Hashtable * ht, uint32_t, int * found);

/**
 * Returns 1 in case the hashtable contains the provided key, otherwise it
 * returns 0.
 */
int hashtable_has_key(Hashtable * ht, uint32_t key);

/**
 * Sets the entry with the given key to the provided value. In case no such
 * entry exists a new entry is created.
 *
 * This function returns 0 in case of success and -1 in case of failure (which
 * should only happen in case there is no memory available).
 */
int hashtable_set(Hashtable * ht, uint32_t key, void * value);

/**
 * Returns the value for a given key.
 * In case of failure it returns NULL and if 'found' is not NULL the integer it
 * points too is also set to 0. In case the key is and 'found' is not null it is
 * set to 1.
 */
void * hashtable_get(Hashtable * ht, uint32_t key, int * found);

/**
 * Returns the number of set keys in the hashtable.
 */
int hashtable_get_load(Hashtable * ht);

/**
 * Returns the number of 'buckets' in the hashtable.
 */
int hashtable_get_size(Hashtable * ht);

/**
 * Iterates over all elements of the hashtable.
 */
void hashtable_iterate(Hashtable * ht, void (*iterator)(uint32_t key, void * data, void * it_data), void * it_data);

/**
 * Generates a report of the hashtable status, useful for debugging.
 * Any undesired field of the report may be set to NULL so that it won't be
 * reported.
 *
 * Field Details:
 * 	- size: number of buckets;
 * 	- load: number of entries;
 * 	- avg_buck_size: average number of entries per bucket;
 * 	- max_buck_size: maximum size over all buckets;
 * 	- mim_buck_size: minimum bucket size;
 * 	- buckets_sizes: pointer to a [preallocated] array to contain the size of
 *		each bucket.
 */
void hashtable_get_status_report(Hashtable * ht, int * size, int * load, int * avg_buck_size, int * max_buck_size,
		int * min_buck_size, int * bucket_sizes);

/**
 * For debug, prints the whole table.
 */
void hashtable_print_all(Hashtable * ht, FILE * stream);

#ifdef _cplusplus
}
#endif

#endif

