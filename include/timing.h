/*******************************************************************************
 * timing.h
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TIMING_H__
#define __TIMING_H__

/**
 * Note that this is not a *robust* library, it's fairly limited.
 * The maximum number of time marks is 128.
 */


/**
 * Returns a positive int for a time mark handle, -1 in case of error.
 */
int time_mark();

/**
 * Returns a double precision floating point value indicating the amount of
 * time passed since the provided mark was set.
 */
double time_get_enlapsed(int h);

/**
 * Resets the mark to the current time.
 *
 * Like a lap on a timer.
 */
void time_reset_mark(int h);

/**
 * Removes a time mark.
 */
void time_clear_mark(int h);


#endif
