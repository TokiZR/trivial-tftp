/*******************************************************************************
 * tftp.h
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TFTP_H__
#define __TFTP_H__

#include <inttypes.h>
#include <time.h>

#include <sys/socket.h>
#include <netinet/in.h>

/*******************************************************************************
 *
 * The almighty Trivial File Transfer Protocol:
 *	(More useless than RFC 2324)
 *
 * This file defines the protocol and it's functionality, it does not however
 * implement a client or server.
 *
 */

/**
 * Maximum size for a packet, no packets can be bigger than this.
 */
#define TFTP_PACKET_SIZE (512 + 4)

/**
 * Default timeouts.
 */
extern const struct timespec TFTP_DEFAULT_RTIMEOUT; /* =  {0, 400000000} */
extern const struct timespec TFTP_DEFAULT_CTIMEOUT; /* =  {2, 0} */

/**
 * Packet type.
 */
typedef enum _TftpPacketType {
	TFTP_RRQ	= 1,
	TFTP_WRQ	= 2,
	TFTP_DATA	= 3,
	TFTP_ACK	= 4,
	TFTP_ERROR	= 5
} TftpPacketType;

/**
 * Error types.
 */
typedef enum _TftpError {
	TFTP_ERROR_UNKNOWN,
	TFTP_ERROR_FILE_NOT_FOUND,
	TFTP_ERROR_ACCESS_VIOLATION,
	TFTP_ERROR_DISK_FULL,
	TFTP_ERROR_ILLEGAL_OPERATION,
	TFTP_ERROR_UNKNOWN_TID,
	TFTP_ERROR_FILE_EXISTS,
	TFTP_ERROR_NO_SUCH_USER
} TftpError;

/*******************************************************************************
 *
 * Messages are exchanged using these packet formats to and from tftp
 * connections.
 *
 */

typedef struct _TftpPacket {
	TftpPacketType type: 16;
} __attribute__((packed)) TftpPacket;


/***************************************
 * Note for requests, only mode supported is octet.
 */

typedef struct _TftpPacketRrq {
	TftpPacket p;
	char path[];
	//char * mode;
} __attribute__((packed)) TftpPacketRrq;

typedef struct _TftpPacketWrq {
	TftpPacket p;
	char path[];
	//char * mode;
} __attribute__((packed)) TftpPacketWrq;

typedef struct _TftpPacketData {
	TftpPacket p;
	uint16_t blockno;
	uint8_t data[];
} __attribute__((packed)) TftpPacketData;

typedef struct _TftpPacketAck {
	TftpPacket p;
	uint16_t blockno;
} __attribute__((packed)) TftpPacketAck;

typedef struct _TftpPacketError {
	TftpPacket p;
	uint16_t err_code;
	char err_msg[];
} __attribute__((packed)) TftpPacketError;

/**
 * Tftp Session:
 * 	Address of a peer, tftp is a 'connectionless' protocol.
 */
typedef struct _TftpSession {
	/**
	 * Foreign address.
	 */
	struct sockaddr_in addr;

	/**
	 * Packet timeout.
	 */
	struct timespec ptimeout;

	/**
	 * Connection timeout.
	 */
	struct timespec ctimeout;
} TftpSession;

/**
 * The server session object, adds the address and socket of the server.
 */
typedef struct _TftpSvrSession TftpSvrSession;

/**
 * A request for a file to either read or write.
 */
typedef struct _TftpRequest {
	/**
	 * Source file for write and destination for read.
	 */
	char * path;

	/**
	 * Remote address.
	 */
	struct sockaddr_in addr;

	/**
	 * Weather the request is a read or write (r = 1, w = 0).
	 */
	int rw: 1;
} TftpRequest;

/*******************************************************************************
 *
 * Client methods.
 *
 */

/**
 * Creates a session for connecting to a server.
 */
TftpSession * tftp_session(uint32_t ip, uint16_t port);

/**
 * Makes a request, either read(0) or write(1), to a given file path.
 *
 * Returns the number of bytes transfered on success and -1 on error.
 * On error the corresponding error code shall be on errno.
 */
int tftp_request(int rw, char * path, char * file, TftpSession * s);

/*******************************************************************************
 *
 * Server methods.
 *
 */

/**
 * Creates a new server socket for a tftp session.
 */
TftpSvrSession * tftp_server(uint16_t port);

/**
 * Polls the server socket for activity and serves any requests that have been
 * cleared to be served.
 * Whenever a new request comes it is returned to the caller so it can decide
 * weather or not to serve it.
 */
TftpRequest * tftp_poll_and_serve(TftpSvrSession * s);

/**
 * Tells the server that this request should be fulfilled.
 */
void tftp_respond(TftpRequest * r, int accept, int err_no, char * emsg);

/**
 * Sets the connection timeout.
 */
void tftp_set_ctimeout(TftpSvrSession * svr, struct timespec tmt);

/**
 * Sets the packet timeout.
 */
void tftp_set_ptimeout(TftpSvrSession * svr, struct timespec tmt);

/*******************************************************************************
 *
 * Misc functions
 *
 */

/**
 * Sets the output of debug messages, if NULL the disable debugging.
 */
void tftp_set_debugging(FILE * out);

/**
 * Enables or disables packet tracing.
 */
void tftp_trace_packets(int trace);

#endif /* __TFTP_H__ */

