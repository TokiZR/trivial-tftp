/*******************************************************************************
 * tftp-priv.h
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TFTP_PRIV_H__
#define __TFTP_PRIV_H__

#define INTERNALS_NAMESPACE /*static*/

extern FILE * __TFTP_DEBUG;

#define DEBUG(msg, ...) do {\
		if(__TFTP_DEBUG) {\
			fprintf(__TFTP_DEBUG, msg, ##__VA_ARGS__);\
		}\
	} while(0)

/**
 * Signal used to deliver timeout events.
 */
#define SIGCTIMEOUT SIGRTMIN
#define SIGPTIMEOUT (SIGRTMIN+1)


/*******************************************************************************
 *
 * Misc functions.
 *
 */

/***************************************
 * Packet sending functions
 */

/**
 * Send a write request.
 */
INTERNALS_NAMESPACE
void tftp_send_packet_rrq(int sock, struct sockaddr_in * addr, char * file);

/**
 * Send a read request.
 */
INTERNALS_NAMESPACE
void tftp_send_packet_wrq(int sock, struct sockaddr_in * addr, char * file);

/**
 * Send an ack packet.
 */
INTERNALS_NAMESPACE
void tftp_send_packet_ack(int sock, struct sockaddr_in * addr, uint16_t blockno);

/**
 * Sends a data packet.
 */
INTERNALS_NAMESPACE
void tftp_send_packet_data(int sock, struct sockaddr_in * addr, uint8_t * bytes, uint16_t blockno, uint16_t len);

/**
 * Send an error packet.
 */
INTERNALS_NAMESPACE
void tftp_send_packet_error(int sock, struct sockaddr_in * addr, uint16_t code, char * message);

/**
 * Reads a packet from a stream.
 *
 * Retry tells weather or not we retry on interrupt.
 */
INTERNALS_NAMESPACE
int tftp_read_packet(int sock, void * buffer, struct sockaddr_in * address, int retry);


#endif
