# Trivial File Transfer Protocol

This is a relatively polished implementation of TFTP featuring:

* Multiplexing Server: while single threaded the server can serve multiple requests simultaneously.
* Multi Threaded Client: the client can make multiple requests at a time, for simplicity each request is made on a separate thread.
* Simple interactive prompt: multiple requests can be made to the server in a single session through a simple command line interface.

## Details

The implementation supports packet tracing (on by default on the server [can be disabled with -DNDEBUG at compile time]) and timeout configuration (through the timeout and rexmt commands).

Otherwise the client and server are fully compliant to the RFC (as far as it has been tested).