/*******************************************************************************
 * timing.c
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "slingshot/util/timing.h"

#include <time.h>
#include <malloc.h>
#include <stdlib.h>

static struct timespec * marks[128] = {0};

static int timespec_subtract(struct timespec *result, struct timespec *x, struct timespec*y) {
	/* Perform the carry for the later subtraction by updating y. */
	if (x->tv_nsec < y->tv_nsec) {
		int nsec = (y->tv_nsec - x->tv_nsec) / 1000000000 + 1;
		y->tv_nsec -= 1000000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_nsec - y->tv_nsec > 1000000000) {
		int nsec = (x->tv_nsec - y->tv_nsec) / 1000000000;
		y->tv_nsec += 1000000000 * nsec;
		y->tv_sec -= nsec;
	}

	/* Compute the time remaining to wait.
	 tv_nsec is certainly positive. */
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_nsec = x->tv_nsec - y->tv_nsec;

	/* Return 1 if result is negative. */
	return x->tv_sec < y->tv_sec;
}

int time_mark() {
	unsigned i;
	for (i = 0; i < 128; i++) {
		if (!marks[i]) {
			marks[i] = malloc(sizeof(struct timespec));
			if (!marks[i]) return -1;
			clock_gettime(CLOCK_MONOTONIC, marks[i]);
			return i;
		}
	}
	return -1;
}

double time_get_enlapsed(int h) {
	double t;
	struct timespec e;

	if (h < 0 || h >= 128 || !marks[h]) return 0.0;

	clock_gettime(CLOCK_MONOTONIC, &e);

	timespec_subtract(&e, &e, marks[h]);

	t = e.tv_sec;
	t += (double) e.tv_nsec / 1000000000;

	return t;
}

void time_reset_mark(int h) {
	if (h >= 0 && h < 128 && marks[h]) {
		clock_gettime(CLOCK_MONOTONIC, marks[h]);
	}
}

void time_clear_mark(int h) {
	if (h >= 0 && h < 128) {
		free(marks[h]);
		marks[h] = 0;
	}
}
