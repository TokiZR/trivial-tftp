/*******************************************************************************
 * client.c
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include "tftp.h"
#include "util.h"
#include "tftp-priv.h"

#define MAX_LINE_LENGTH 2048

/***************************************
 *	Commands
 */

#define cmd_error(fmt, ...)\
	do{\
		char * msg;\
		asprintf(&msg, fmt, ##__VA_ARGS__);\
		return msg;\
	} while(0)

char * cmd_quit(int argc, char ** argv);
char * cmd_get(int argc, char ** argv);
char * cmd_put(int argc, char ** argv);
char * cmd_trace(int argc, char ** argv);
char * cmd_rexmt(int argc, char ** argv);
char * cmd_timeout(int argc, char ** argv);
char * cmd_help(int argc, char ** argv);

typedef struct {
	char * name;
	char * desc;
	char * (*command)(int argc, char ** argv);
} Command;

/**
 * Supported commands.
 *
 * Shoul be self describing.
 */
Command cmdtable[] = {
		{"quit", "Exit the client.", cmd_quit},
		{"get", "Get a file from the server.", cmd_get},
		{"put", "Send a file to the server.", cmd_put},
		{"trace", "Enable packet tracing.", cmd_trace},
		{"rexmt", "Set packet timeout.", cmd_rexmt},
		{"timeout", "Set connection timeout.", cmd_timeout},
		{"help", "Print this help listing.", cmd_help}
};

int cmdlen = (sizeof(cmdtable) / sizeof(Command));

/**
 * Global so commands can reach it.
 */
TftpSession * session;

int main(int argc, char ** argv) {
	int i;
	char linebuff[MAX_LINE_LENGTH] = {0};

	/* Debugging */
	//tftp_set_debugging(stdout);
	if (argc != 3) {
		fprintf(stderr, "Error, expected 2 arguments: <host> and <port> to connect to.\n");
		return 1;
	}

	struct addrinfo hints;
	struct addrinfo * result;
	int eno;

	/* Obtain address(es) matching host/port */

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET; /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
	hints.ai_flags = 0;
	hints.ai_protocol = 0; /* Any protocol */

	if ((eno = getaddrinfo(argv[1], argv[2], &hints, &result)) < 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(eno));
		exit(1);
	}

	while (result->ai_family != AF_INET) {
		result = result->ai_next;
		if (!result)
			fprintf(stderr, "Could not locate a valid IPv4 address for the given host\n");

		return 1;
	}

	struct sockaddr_in * addr = (struct sockaddr_in *) result->ai_addr;
	session = tftp_session(ntohl(addr->sin_addr.s_addr), ntohs(addr->sin_port));

	puts("Simple TFTP Client v0.1");
	printf("Bound to contact %s\n", atop(addr));

	/* Client input loop */
	while (1) {
		printf("tftp>");
		if (fgets(linebuff, MAX_LINE_LENGTH, stdin)) {
			if (linebuff[MAX_LINE_LENGTH - 2] != '\n' && linebuff[MAX_LINE_LENGTH - 2] != '\0') {
				/* Not a line break or NULL byte, indicates a line overflow. */
				linebuff[MAX_LINE_LENGTH - 2] = 0;
				fprintf(stderr, "Error: Command line is too long.\n");
				continue;
			}

			/* Parse input line */
			int cargc;
			char ** cargv;
			char * wd, *cmd;

			cargc = 0;
			cargv = NULL;

			cmd = strtok(linebuff, " \t\n");

			if (!cmd)
				continue;

			while ((wd = strtok(NULL, " \t\n"))) {
				cargc++;
				cargv = realloc(cargv, sizeof(char *) * cargc);
				cargv[cargc - 1] = wd;
			}

			/* Find command */

			Command *c = NULL;

			for (i = 0; i < cmdlen; i++) {
				if (strcmp(cmdtable[i].name, cmd) == 0) {
					c = cmdtable + i;
				}
			}

			if (c) {
				char * ret;
				ret = c->command(cargc, cargv);

				if (ret) {
					fprintf(stderr, "%s\n", ret);
					free(ret);
				}
			} else {
				fprintf(stderr, "Unrecognized command '%s'.\n", cmd);
			}
		}
	}

	return 0;
}

char * cmd_quit(int argc, char ** argv) {
	exit(0);
}

char * cmd_get(int argc, char ** argv) {
	int i;
	pthread_t ths[argc];
	if (argc < 1) cmd_error("Expecting at least one remote file path.");
	void * do_get(char * path) {
		int size;
		if ((size = tftp_request(0, path, path, session)) > 0) {
			printf("Successfully received \"%s\" (%d %s)\n", path, size, size > 1 ? "bytes" : "byte");
		}
		return NULL;
	}

	for (i = 0; i < argc; i++) {
		pthread_create(&ths[i], NULL, (void * (*)(void *)) do_get, argv[i]);
	}

	for (i = 0; i < argc; i++) {
		pthread_join(ths[i], NULL);
	}

	return NULL;
}

char * cmd_put(int argc, char ** argv) {
	int i;
	pthread_t ths[argc];
	if (argc < 1) cmd_error("Expecting at least one remote file path.");
	void * do_put(char * path) {
		int size;
		if ((size = tftp_request(1, path, path, session)) > 0) {
			printf("Successfully sent \"%s\" (%d %s)\n", path, size, size > 1 ? "bytes" : "byte");
		}
		return NULL;
	}

	for (i = 0; i < argc; i++) {
		pthread_create(&ths[i], NULL, (void * (*)(void *)) do_put, argv[i]);
	}

	for (i = 0; i < argc; i++) {
		pthread_join(ths[i], NULL);
	}

	return NULL;
}

char * cmd_trace(int argc, char ** argv) {
	static int trace = 0;
	trace = !trace;

	puts(trace ? "Tracing enabled" : "Tracing disabled");

	tftp_trace_packets(trace);

	return NULL;
}

char * cmd_rexmt(int argc, char ** argv) {
	if (argc != 1) cmd_error("Warning: Expecting one and only one argument: <timeout>");

	session->ptimeout.tv_nsec = 0;
	session->ptimeout.tv_sec = atoi(argv[0]);

	return NULL;
}

char * cmd_timeout(int argc, char ** argv) {
	if (argc != 1) cmd_error("Warning: Expecting one and only one argument: <timeout>");

	session->ctimeout.tv_nsec = 0;
	session->ctimeout.tv_sec = atoi(argv[0]);

	return NULL;
}

char * cmd_help(int argc, char ** argv) {
	puts("Available Commands:");

	for (int i = 0; i < cmdlen; i++) {
		printf("\t%s - %s\n", cmdtable[i].name, cmdtable[i].desc);
	}

	return NULL;
}