/*******************************************************************************
 * server.c
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>

#include "tftp.h"

int main(int argc, char ** argv) {
	TftpSvrSession * svr;
	TftpRequest * req;
	int fd;

	if (argc < 2) {
		fprintf(stderr, "Expecting at least one argument <port>\n");
		return 1;
	}

	puts("TFTP Server v0.1");

	//tftp_set_debugging(stdout);
	tftp_trace_packets(1);

	svr = tftp_server(atoi(argv[1]));
	if (!svr) exit(1);

	while (1) {
		req = tftp_poll_and_serve(svr);
		if (req) {
			if ((fd = open(req->path, req->rw ? O_RDWR | O_CREAT | O_TRUNC : O_RDONLY, 0666)) >= 0) {
				close(fd);
				tftp_respond(req, 1, 0, NULL);
				continue;
			}

			switch (errno) {
				case EACCES:
					case EROFS:
					tftp_respond(req, 0, TFTP_ERROR_ACCESS_VIOLATION, "Access denied.");
					break;
				case ENAMETOOLONG:
					case ENODEV:
					case EISDIR:
					case ELOOP:
					case ENOTDIR:
					case ENOENT:
					tftp_respond(req, 0, TFTP_ERROR_FILE_NOT_FOUND, "File not found.");
					break;
			}
		}
	}

	return 0;
}
