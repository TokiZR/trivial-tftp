/*******************************************************************************
 * tftp.c
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <poll.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "tftp.h"
#include "util.h"
#include "tftp-priv.h"

/**
 * Default timeouts.
 */
const struct timespec TFTP_DEFAULT_RTIMEOUT = {0, 400000000};
const struct timespec TFTP_DEFAULT_CTIMEOUT = {2, 0};
struct _TftpSessionData {

} TftpSessionData;

FILE * __TFTP_DEBUG = NULL;

/* Tracing */
static int enable_tracing;

/*******************************************************************************
 *
 *	Misc implementation
 *
 */

INTERNALS_NAMESPACE
void tftp_send_packet_rrq(int sock, struct sockaddr_in * addr, char * file) {
	uint8_t buffer[TFTP_PACKET_SIZE];
	TftpPacketRrq * p = (TftpPacketRrq *) buffer;

	/* Trace */
	if (enable_tracing) {
		printf("<RRQ \"%s\">\n", file);
	}

	/* Prepare packet */
	p->p.type = htons(TFTP_RRQ);
	strcpy(p->path, file);
	strcpy((char *) (buffer + sizeof(*p) + strlen(file) + 1), "octet");

	/* Send packet */
	sendto(sock, buffer, sizeof(*p) + strlen(p->path) + 1, 0, addr, sizeof(*addr));
}

INTERNALS_NAMESPACE
void tftp_send_packet_wrq(int sock, struct sockaddr_in * addr, char * file) {
	uint8_t buffer[TFTP_PACKET_SIZE];
	TftpPacketWrq * p = (TftpPacketWrq *) buffer;

	/* Trace */
	if (enable_tracing) {
		printf("<WRQ \"%s\">\n", file);
	}

	/* Prepare packet */
	p->p.type = htons(TFTP_WRQ);
	strcpy(p->path, file);
	strcpy((char *) (buffer + sizeof(*p) + strlen(file) + 1), "octet");

	/* Send packet */
	sendto(sock, buffer, sizeof(*p) + strlen(p->path) + 1, 0, addr, sizeof(*addr));
}

INTERNALS_NAMESPACE
void tftp_send_packet_ack(int sock, struct sockaddr_in * addr, uint16_t blockno) {
	uint8_t buffer[TFTP_PACKET_SIZE];
	TftpPacketAck * p = (TftpPacketAck *) buffer;

	/* Trace */
	if (enable_tracing) {
		printf("<ACK %hu>\n", blockno);
	}

	/* Prepare packet */
	p->p.type = htons(TFTP_ACK);
	p->blockno = htons(blockno);

	/* Send packet */
	sendto(sock, buffer, sizeof(*p), 0, addr, sizeof(*addr));
}

INTERNALS_NAMESPACE
void tftp_send_packet_data(int sock, struct sockaddr_in * addr, uint8_t * bytes, uint16_t blockno, uint16_t len) {
	uint8_t buffer[TFTP_PACKET_SIZE];
	TftpPacketData * p = (TftpPacketData *) buffer;

	/* Trace */
	if (enable_tracing) {
		printf("<DATA %hu, %hu bytes>\n", blockno, len);
	}

	/* Prepare packet */
	p->p.type = htons(TFTP_DATA);
	p->blockno = htons(blockno);
	memcpy(p->data, bytes, len);

	/* Send packet */
	sendto(sock, buffer, sizeof(*p) + len, 0, addr, sizeof(*addr));
}

INTERNALS_NAMESPACE
void tftp_send_packet_error(int sock, struct sockaddr_in * addr, uint16_t code, char * message) {
	uint8_t buffer[TFTP_PACKET_SIZE];
	TftpPacketError * p = (TftpPacketError *) buffer;

	/* Trace */
	if (enable_tracing) {
		printf("<ERROR %hu:\"%s\">\n", code, message);
	}

	/* Prepare packet */
	p->p.type = htons(TFTP_ERROR);
	p->err_code = htons(code);
	strcpy(p->err_msg, message);

	/* Send packet */
	sendto(sock, buffer, sizeof(*p) + strlen(p->err_msg) + 1, 0, addr, sizeof(*addr));
}

INTERNALS_NAMESPACE

int tftp_read_packet(int sock, void * buffer, struct sockaddr_in * address, int retry) {

	int plen;
	socklen_t addrlen = sizeof(*address);
	TftpPacket * p;

	/* Read request */
	retry: ;
	plen = recvfrom(sock, buffer, TFTP_PACKET_SIZE, 0, address, &addrlen);

	if (plen < 4) {
		if (plen < 0) {
			/* If interrupted then it was likely just a timer and we retry */

			if (errno == EINTR && retry)
				goto retry;

			perror("Error receiving packet");
		} else
			fprintf(stderr, "Packet was too small");
		return -1;
	}

	p = ((TftpPacket *) buffer);

	p->type = ntohs(p->type);

	/* Do byte order translation coz otherwise I will hate myself forever */
	switch (p->type) {
		case TFTP_ACK:
			;
			TftpPacketAck * pa = (TftpPacketAck *) p;
			pa->blockno = ntohs(pa->blockno);
			break;
		case TFTP_DATA:
			;
			TftpPacketData * pd = (TftpPacketData *) p;
			pd->blockno = ntohs(pd->blockno);
			break;
		case TFTP_ERROR:
			;
			TftpPacketError * pe = (TftpPacketError *) p;
			pe->err_code = ntohs(pe->err_code);
			break;
	}

	DEBUG("Received packet from %s", atop(address));

	if (enable_tracing) {
		if (__TFTP_DEBUG) printf(": ");
		switch (p->type) {
			case TFTP_RRQ:
				;
				TftpPacketRrq * pr = (TftpPacketRrq *) p;
				if (enable_tracing) {
					printf("<RRQ \"%s\">", pr->path);
				}
				break;
			case TFTP_WRQ:
				;
				TftpPacketWrq * pw = (TftpPacketWrq *) p;
				if (enable_tracing) {
					printf("<WRQ \"%s\">", pw->path);
				}
				break;
			case TFTP_DATA:
				;
				TftpPacketData * pd = (TftpPacketData *) p;
				if (enable_tracing) {
					printf("<DATA %hu, %hu bytes>", pd->blockno, plen - 4);
				}
				break;
			case TFTP_ACK:
				;
				TftpPacketAck * pa = (TftpPacketAck *) p;
				if (enable_tracing) {
					printf("<ACK %hu>", pa->blockno);
				}
				break;
			case TFTP_ERROR:
				;
				TftpPacketError * pe = (TftpPacketError *) p;
				if (enable_tracing) {
					printf("<ERROR %hu:\"%s\">", pe->err_code, pe->err_msg);
				}
				break;
		}
		putchar('\n');
	} else if (__TFTP_DEBUG) {
		putchar('\n');
	}

	return plen;
}

/*******************************************************************************
 *
 * Client Library functions.
 *
 */

TftpSession * tftp_session(uint32_t ip, uint16_t port) {
	TftpSession * s = malloc(sizeof(TftpSession));

	memset(&s->addr, 0, sizeof(s->addr));
	s->addr.sin_family = AF_INET;
	s->addr.sin_port = htons(port);
	s->addr.sin_addr.s_addr = htonl(ip);
	s->ptimeout = TFTP_DEFAULT_RTIMEOUT;
	s->ctimeout = TFTP_DEFAULT_CTIMEOUT;

	return s;
}

/*******************************************************************************
 *
 *	Client transfer operation, returns when the transfer is finished.
 *
 * Uses signal SIGRTMIN.
 *
 */

typedef struct {
	uint8_t * buffer;
	uint16_t blen;

	uint16_t blockno;

	int sock;
	TftpSession * s;

	unsigned done :1;
	unsigned tmt :1;
} TftpCReq;

/**
 * Handler for timely timer events.
 *
 * We use poll to do the packet timeout so we only have one timer for the
 * connection.
 */
static void tftp_ctimer_handler(int sig, siginfo_t * si, void * data) {
	TftpCReq * req;

	req = (TftpCReq *) si->_sifields._timer.si_sigval.sival_ptr;
	req->tmt = 1;
}

int tftp_request(int rw, char * path, char * lfile, TftpSession * s) {
	uint8_t buffer[TFTP_PACKET_SIZE];
	int sock;
	int len, file;
	TftpCReq req;
	timer_t ctimer;
	int fsize = 0;

	/* Simpler to have this here */
	void reset_timer() {
		struct itimerspec its;

		its.it_value = its.it_interval = s->ctimeout;
		//timer_settime(ctimer, 0, &its, NULL);
	}

	/* Open new socket to talk to remote host */

	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("Cannot open connection socket");
		return -1;
	}

	/* 'connect' */

	if (connect(sock, &s->addr, sizeof s->addr) < 0) {
		perror("Cannot connect to remote host");
		return -1;
	}

	req.sock = sock;
	/* Writes wait for ack0, so their blockno starts at 0 */
	req.blockno = !rw;
	req.s = s;
	req.done = 0;
	req.tmt = 0;
	req.blen = 0;
	req.buffer = NULL;

	file = open(lfile, !rw ? O_CREAT | O_RDWR | O_TRUNC : O_RDONLY, 0666);
	if (file < 0) {
		perror("Cannot open local file");
		return -1;
	}

	/* Setup timeout timer */
	struct sigevent evt;
	evt.sigev_notify = SIGEV_SIGNAL;
	evt.sigev_signo = SIGCTIMEOUT;
	evt.sigev_value.sival_ptr = &req;

	timer_create(CLOCK_MONOTONIC, &evt, &ctimer);

	/* Setup signal handler */
	struct sigaction sa;

	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = tftp_ctimer_handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGCTIMEOUT, &sa, NULL) < 0) {
		perror("Could not register signal handlers");
		goto error;
	}

	if (rw) {
		DEBUG("Sending a write request for '%s' to remote host %s.\n", path, atop(&s->addr));
		tftp_send_packet_wrq(sock, &s->addr, path);

		/* Allocate cache buffer for data packets */
		req.buffer = malloc(512);
	} else {
		DEBUG("Sending a read request for '%s' to remote host %s.\n", path, atop(&s->addr));
		tftp_send_packet_rrq(sock, &s->addr, path);
	}

	/* Fire connection timer */
	reset_timer();

	struct pollfd pfds = {sock, POLLIN, 0};
	uint64_t tmt = s->ptimeout.tv_sec * 1000 + s->ptimeout.tv_nsec / 1000000;

	while (1) {
		/* When done we just leave */
		if (req.done)
			break;

		/* Poll the descriptor */
		switch (poll(&pfds, 1, tmt)) {
			case 0:
				/* Retry */
				if (rw) {
					tftp_send_packet_data(req.sock, &req.s->addr, req.buffer, req.blockno, req.blen);
				} else {
					tftp_send_packet_ack(req.sock, &req.s->addr, req.blockno - 1);
				}
				continue;
				break;
			case -1:
				fprintf(stderr, "Connection timed out\n");
				goto error;
				break;
		}
		len = tftp_read_packet(sock, buffer, &s->addr, 0);
		/* It could still get intewwrrupted */
		if (len == -1 && req.tmt) {
			fprintf(stderr, "Connection timed out\n");
			goto error;
		}

		TftpPacket * p;
		p = (TftpPacket *) buffer;

		/* Check for error, error at the end means transmission finished successfully */
		if (p->type == TFTP_ERROR) {
			TftpPacketError * pe = (TftpPacketError *) p;
			fprintf(stderr, "Got error %d: %s\n", pe->err_code, pe->err_msg);
			goto error;
		}

		/* Process based on the transaction type */

		if (rw) {
			/* Write */
			if (p->type == TFTP_ACK) {
				TftpPacketAck * pa = (TftpPacketAck *) p;

				if (pa->blockno == req.blockno) {
					req.blen = pread(file, req.buffer, 512, 512 * req.blockno);
					fsize += req.blen;
					if (req.blen < 512)
						req.done = 1;
					req.blockno++;
					tftp_send_packet_data(req.sock, &req.s->addr, req.buffer, req.blockno, req.blen);
				} else if (pa->blockno > req.blockno) {
					tftp_send_packet_error(req.sock, &req.s->addr, TFTP_ERROR_ILLEGAL_OPERATION, "Block number mismatch");
					goto error;
				}

			} else {
				tftp_send_packet_error(req.sock, &req.s->addr, TFTP_ERROR_ILLEGAL_OPERATION, "Expecting an Ack packet");
				goto error;
			}
		} else {
			/* Read */
			if (p->type == TFTP_DATA) {
				TftpPacketData * pd = (TftpPacketData *) p;

				if (pd->blockno == req.blockno) {
					/* Matching block number, write it */
					fsize += len - sizeof(*pd);
					write(file, pd->data, len - sizeof(*pd));
					tftp_send_packet_ack(req.sock, &req.s->addr, req.blockno);
					req.blockno++;
					if (len < TFTP_PACKET_SIZE) {
						req.done = 1;
					}
				} else if (pd->blockno > req.blockno) {
					/* This blockno is evil */
					DEBUG("Expected block number %d but received %d\n", req.blockno, pd->blockno);
					tftp_send_packet_error(req.sock, &req.s->addr, TFTP_ERROR_ILLEGAL_OPERATION, "Block number mismatch");
					goto error;
				}

			} else {
				tftp_send_packet_error(req.sock, &req.s->addr, TFTP_ERROR_ILLEGAL_OPERATION, "Expecting a Data packet");
				goto error;
			}
		}

		/* If got here then it all went fine, so we reset the timer */
		reset_timer();

	}

	/* Normal finalization */
	if (req.buffer)
		free(req.buffer);
	close(file);
	timer_delete(ctimer);

	return fsize;

	/* Error case */
	error: if (req.buffer)
		free(req.buffer);
	close(file);
	timer_delete(ctimer);
	/* if read then unlink */
	if (!rw) unlink(path);
	return -1;
}

void tftp_set_debugging(FILE * out) {
	__TFTP_DEBUG = out;
}

void tftp_trace_packets(int trace) {
	enable_tracing = trace;
}
