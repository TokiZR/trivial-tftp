/*******************************************************************************
 * tftp-server.c
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _STDC_FORMAT_MACROS

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>

#include <sys/select.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include "tftp.h"
#include "util.h"
#include "tftp-priv.h"
#include "hashtable.h"

/*******************************************************************************
 *
 * The TFTP server keeps a list of ongoing requests.
 * Whenever a packet arrives the origin is checked against the internal list
 * and if a match is found the packet is routed for processing.
 *
 * When a unknown address makes sends a packet it is checked for a request, if
 * that is the case then the request is passed onto the parent context which
 * gets to decide if the request should be fulfilled. Otherwise an error
 * response is sent to that source.
 *
 * Since the server runs over the UDP protocol error checking is done manually,
 * for that reason requests have a state machine on them that maintains
 * information about data validation.
 *
 */

/***************************************
 * Simple macros for 'locking'.
 *
 * Locking here is simply setting a control variable to prevent signal handlers
 * from being evil.
 */
#define svr_ht_lock(svr) do {\
	asm volatile("" ::: "memory");\
	svr->busy = 1;\
	asm volatile("" ::: "memory");\
} while(0)

#define svr_ht_unlock(svr) do {\
	asm volatile("" ::: "memory");\
	svr->busy = 0;\
	tftp_svr_flush_dead(svr);\
	asm volatile("" ::: "memory");\
} while(0)

#define req_lock(req) do {\
	asm volatile("" ::: "memory");\
	req->busy = 1;\
	asm volatile("" ::: "memory");\
} while(0)

#define req_unlock(req) do {\
	asm volatile("" ::: "memory");\
	req->busy = 0;\
	asm volatile("" ::: "memory");\
} while(0)

/***************************************
 * Data structures
 */
typedef struct _TftpReq TftpReq;

struct _TftpReq {
	TftpRequest data;

	/* File that is being read or written */
	int file;

	/**
	 * The server session responsible for this request.
	 */
	TftpSvrSession * svr;

	TftpReq * next;

	/**
	 * Buffer used on read requests.
	 */
	uint8_t * buffer;

	/**
	 * Number of used bytes on the buffer (for read requests).
	 */
	short blen;

	/**
	 * Timer for the packet timeout.
	 */
	timer_t ptimer;

	/**
	 * Timer for the connection timeout
	 */
	timer_t ctimer;

	/* Current block number */
	uint16_t blockno;

	/* When a timeout occurs this flag is checked and if set the timeout is
	 * postponed */
	volatile unsigned busy :1;

	/* When the request finishes it waits until timeout in case the peer may
	 * loose the final ack.
	 */
	volatile unsigned done :1;
};

struct _TftpSvrSession {
	TftpSession s;

	/**
	 * Connection socket.
	 */
	int sock;

	/**
	 * Active requests.
	 */
	Hashtable * requests;

	/* A linked list of dead requests, dead requests are those that timeout
	 * while the server hashtable was busy and therefore could not be removed.
	 */
	TftpReq * dead;

	/**
	 * A hashtable 'lock' to prevent signal handlers from doing bad things.
	 */
	unsigned busy :1;
};

/**
 * The handler for timer events used to implement timeouts.
 */
static void tftp_timer_handler(int sig, siginfo_t * si, void * data);

/**
 * Function used to handle al;l the request state and reponse management.
 */
static void tftp_request_serve(TftpReq * req, TftpPacket * p, int pkt_size);

/**
 * Finalises a request.
 */
static void tftp_request_finalize(TftpReq * req);

/**
 * Timer misc functions
 */

static void tftp_req_reset_timers(TftpReq * req);

/**
 * Removes requests marked as dead but not removed from the hashtable because
 * of concurrency issues.
 */
static void tftp_svr_flush_dead(TftpSvrSession * s);

/**
 * Handler for the timer events.
 */
static void tftp_timer_handler(int sig, siginfo_t * si, void * data) {
	TftpReq * req;
	req = (TftpReq *) si->_sifields._timer.si_sigval.sival_ptr;

	/* If the request is busy this timeout is ignored */
	if (req->busy)
		return;

	/* Which timer is this */
	if (si->si_signo == SIGCTIMEOUT) {

		/* Connection timeout */
		/* End connection */
		DEBUG("Transaction %hu timed out, finalizing\n", req->data.addr.sin_port);
		tftp_request_finalize(req);
	} else {
		/* packet timeout, resend */
		if (req->data.rw) {
			/* write */
			tftp_send_packet_ack(req->svr->sock, &req->data.addr, req->blockno - 1);

		} else {
			/* read */
			tftp_send_packet_data(req->svr->sock, &req->data.addr, req->buffer, req->blockno, req->blen);
		}
	}
}

static void tftp_request_finalize(TftpReq * req) {
	DEBUG("Finalizing transaction %hu\n", req->data.addr.sin_port);

	/* If busy this is an interrupt handler, meaning it will stay busy until we
	 * return */
	if (req->svr->busy) {
		req->next = req->svr->dead;
		req->svr->dead = req;
	} else {
		hashtable_remove(req->svr->requests, req->data.addr.sin_port, NULL);

		close(req->file);
		if (!req->done && req->data.rw)
			unlink(req->data.path);
		free(req->data.path);
		timer_delete(req->ctimer);
		timer_delete(req->ptimer);
		free(req);
	}

}

static void tftp_req_reset_timers(TftpReq * req) {
	struct itimerspec its;

	its.it_value = its.it_interval = req->svr->s.ctimeout;
	DEBUG("Resetting connection timer to %u.%09" PRIu64 "\n", (unsigned ) req->svr->s.ctimeout.tv_sec,
			(uint64_t ) req->svr->s.ctimeout.tv_nsec);
	timer_settime(req->ctimer, 0, &its, NULL);

	its.it_value = its.it_interval = req->svr->s.ptimeout;
	DEBUG("Resetting packet timer to %u.%09" PRIu64 "\n", (unsigned ) req->svr->s.ptimeout.tv_sec,
			(uint64_t ) req->svr->s.ptimeout.tv_nsec);
	timer_settime(req->ptimer, 0, &its, NULL);
}

static void tftp_svr_flush_dead(TftpSvrSession * s) {
	TftpReq * req, *g;

	req = s->dead;
	while (req) {
		g = req;
		req = req->next;

		hashtable_remove(s->requests, g->data.addr.sin_port, NULL);
		close(g->file);
		if (!g->done && req->data.rw)
			unlink(g->data.path);
		free(g->data.path);
		timer_delete(g->ctimer);
		timer_delete(g->ptimer);
		free(g);
	}
}

TftpSvrSession * tftp_server(uint16_t port) {
	TftpSvrSession * sess = malloc(sizeof(TftpSvrSession));

	/* Setup session. */
	memset(&sess->s.addr, 0, sizeof(sess->s.addr));
	sess->s.addr.sin_family = AF_INET;
	sess->s.addr.sin_port = htons(port);
	sess->s.addr.sin_addr.s_addr = htonl(INADDR_ANY);
	sess->s.ptimeout = TFTP_DEFAULT_RTIMEOUT;
	sess->s.ctimeout = TFTP_DEFAULT_CTIMEOUT;
	sess->requests = hashtable_new(101);
	sess->busy = 0;
	sess->dead = NULL;

	DEBUG("Starting server...\n");

	unsigned ctmt, ptmt;
	ctmt = TFTP_DEFAULT_CTIMEOUT.tv_sec * 1000 + TFTP_DEFAULT_CTIMEOUT.tv_nsec / 1000000;
	ptmt = TFTP_DEFAULT_RTIMEOUT.tv_sec * 1000 + TFTP_DEFAULT_RTIMEOUT.tv_nsec / 1000000;

	DEBUG("Setting timeouts(ms) to: ctmt -> %u, ptmt -> %u\n", ctmt, ptmt);

	DEBUG("Opening socket...\n");

	/* Open socket */
	if ((sess->sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("Could not open server socket");
		goto error;
	}

	DEBUG("Binding socket...\n");

	/* Bind socket */
	if (bind(sess->sock, &sess->s.addr, sizeof(struct sockaddr_in)) < 0) {
		perror("Could not bind IP socket");
		goto error;
	}

	DEBUG("Setting up timer handler.\n");
	struct sigaction sa;

	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = tftp_timer_handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGCTIMEOUT, &sa, NULL) < 0) {
		perror("Could not register signal handlers");
		goto error;
	}
	if (sigaction(SIGPTIMEOUT, &sa, NULL) < 0) {

		perror("Could not register signal handlers");
		goto error;
	}

	DEBUG("Done\n");

	/* Return socket */
	return sess;

	/* Handle errors */
	error: if (sess->sock) {
		close(sess->sock);
	}

	free(sess);
	return NULL;
}

static void tftp_request_serve(TftpReq * req, TftpPacket * p, int pkt_size) {
	if (req->data.rw) {
		/* write request */

		if (p->type == TFTP_DATA) {
			TftpPacketData * pd = (TftpPacketData *) p;

			/* Implicitly if a block number is less than blockno -1 we ignore
			 * the packet.
			 */

			if (pd->blockno == req->blockno) {
				if (req->done) {
					tftp_send_packet_ack(req->svr->sock, &req->data.addr, req->blockno);
					return;
				}
				/* Matching block number, write it */
				write(req->file, pd->data, pkt_size - sizeof(*pd));
				tftp_send_packet_ack(req->svr->sock, &req->data.addr, req->blockno);
				tftp_req_reset_timers(req);
				if (pkt_size < TFTP_PACKET_SIZE) {
					req->done = 1;
				}
				req->blockno++;
			} else if (pd->blockno > req->blockno) {
				/* This blockno is evil */
				DEBUG("Expected block number %d but received %d\n", req->blockno, pd->blockno);
				tftp_send_packet_error(req->svr->sock, &req->data.addr, TFTP_ERROR_ILLEGAL_OPERATION, "Block number mismatch");
				req_unlock(req);
				tftp_request_finalize(req);

			}
		} else {
			/* Wrong packet type */
			tftp_send_packet_error(req->svr->sock, &req->data.addr, TFTP_ERROR_ILLEGAL_OPERATION, "Expecting a data packet");
			req_unlock(req);
			tftp_request_finalize(req);

		}
	} else {
		/* Read request */

		if (p->type == TFTP_ACK) {
			TftpPacketAck * pa = (TftpPacketAck *) p;

			/* Implicitly if a block number is less than blockno we ignore
			 * the packet.
			 */

			if (pa->blockno == req->blockno) {
				if (req->done) {
					req_unlock(req);
					tftp_request_finalize(req);
					return;
				}
				/* Matching block number, go on */
				req->blen = pread(req->file, req->buffer, 512, 512 * req->blockno);
				req->blockno++;
				tftp_send_packet_data(req->svr->sock, &req->data.addr, req->buffer, req->blockno, req->blen);
				tftp_req_reset_timers(req);
				if (req->blen < 512) {
					req->done = 1;
				}
			} else if (pa->blockno > req->blockno) {
				/* This blockno is evil */
				tftp_send_packet_error(req->svr->sock, &req->data.addr, TFTP_ERROR_ILLEGAL_OPERATION, "Block number mismatch");
				req_unlock(req);
				tftp_request_finalize(req);
			}

		} else {
			tftp_send_packet_error(req->svr->sock, &req->data.addr, TFTP_ERROR_ILLEGAL_OPERATION, "Expecting an Ack packet");
			req_unlock(req);
			tftp_request_finalize(req);

		}
	}

	req_unlock(req);
}

TftpRequest * tftp_poll_and_serve(TftpSvrSession * s) {
	TftpReq * req = NULL;
	TftpRequest * request;
	TftpPacket * p;
	uint8_t buffer[TFTP_PACKET_SIZE + 1];
	struct sockaddr_in addr;
	int len;

	read_packet: len = tftp_read_packet(s->sock, buffer, &addr, 1);

	if (len < 0) {
		perror("Cannot read from socket");
		return NULL;
	}

	/* Ensure there is a null byte to end the path string. */
	buffer[TFTP_PACKET_SIZE] = 0;

	p = (TftpPacket *) buffer;

	svr_ht_lock(s);
	req = hashtable_get(s->requests, addr.sin_port, NULL);
	if (req) {
		/* Existing transaction */

		/* Check host ip */
		if (req->data.addr.sin_addr.s_addr != addr.sin_addr.s_addr) {
			svr_ht_unlock(s);
			tftp_send_packet_error(s->sock, &addr, TFTP_ERROR_ILLEGAL_OPERATION,
					"Transaction id already in use, please retry.");
		} else {
			req_lock(req);
			svr_ht_unlock(req->svr);
			tftp_request_serve(req, p, len);
		}
		goto read_packet;
	} else {
		switch (p->type) {
			case TFTP_RRQ:
				;
				TftpPacketRrq * pr = (TftpPacketRrq *) buffer;
				req = malloc(sizeof(TftpReq));
				request = &req->data;
				request->addr = addr;
				request->path = strdup(pr->path);
				request->rw = 0;
				break;
			case TFTP_WRQ:
				;
				TftpPacketWrq * pw = (TftpPacketWrq *) buffer;
				req = malloc(sizeof(TftpReq));
				request = &req->data;
				request->addr = addr;
				request->path = strdup(pw->path);
				request->rw = 1;
				break;
			default:
				tftp_send_packet_error(s->sock, &addr, TFTP_ERROR_ILLEGAL_OPERATION, "Invalid operation id.");
				break;
		}
	}

	if (req) {
		req->busy = 0;
		req->svr = s;
		req->done = 0;
		/* Write starts at 1 since ack0 is the handshake response */
		req->blockno = 1;
		req->next = NULL;
	}

	/* We only return if a valid request was received */
	return &req->data;
}

/**
 * This pretty much does all the work.
 */
void tftp_respond(TftpRequest * r, int accept, int err_no, char * emsg) {
	TftpReq * req = (TftpReq *) r;
	TftpSvrSession * s;
	int succ, file;

	s = req->svr;

	if (accept) {
		file = open(r->path, r->rw ? O_CREAT | O_RDWR : O_RDONLY, 0666);

		/* We suppose this should have been tested and just quietly die. */
		if (file < 0)
			goto cleanup;

		req->file = file;

		svr_ht_lock(s);
		succ = hashtable_insert(s->requests, req->data.addr.sin_port, req);
		svr_ht_unlock(s);

		if (succ < 0) {
			goto cleanup;
		}

		/* send first packet */
		if (req->data.rw) {
			tftp_send_packet_ack(s->sock, &req->data.addr, 0);
		} else {
			req->buffer = malloc(512);
			req->blen = read(file, req->buffer, 512);
			tftp_send_packet_data(s->sock, &req->data.addr, req->buffer, req->blockno, req->blen);
		}

		/* Setup timer */
		struct sigevent evt;
		evt.sigev_notify = SIGEV_SIGNAL;
		evt.sigev_signo = SIGCTIMEOUT;
		evt.sigev_value.sival_ptr = req;

		timer_create(CLOCK_MONOTONIC, &evt, &req->ctimer);

		evt.sigev_signo = SIGPTIMEOUT;
		timer_create(CLOCK_MONOTONIC, &evt, &req->ptimer);

		tftp_req_reset_timers(req);

		return;
	} else {
		tftp_send_packet_error(s->sock, &req->data.addr, err_no, emsg);
	}

	cleanup: close(file);

	free(req->data.path);
	free(req);
}

void tftp_set_ctimeout(TftpSvrSession * svr, struct timespec tmt) {
	svr->s.ctimeout = tmt;

	hashtable_iterate(svr->requests, lambda(void, (uint32_t key, void * data, void * it_data) {
				TftpReq * req = data;
				struct itimerspec its;

				TftpSvrSession * s = req->svr;

				its.it_value = its.it_interval = s->s.ctimeout;
				timer_settime(req->ctimer, 0, &its, NULL);
			}), 0);
}

void tftp_set_ptimeout(TftpSvrSession * svr, struct timespec tmt) {
	svr->s.ptimeout = tmt;

	hashtable_iterate(svr->requests, lambda(void, (uint32_t key, void * data, void * it_data) {
				TftpReq * req = data;
				struct itimerspec its;

				TftpSvrSession * s = req->svr;

				its.it_value = its.it_interval = s->s.ptimeout;
				timer_settime(req->ptimer, 0, &its, NULL);
			}), 0);
}
