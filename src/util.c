/*******************************************************************************
 * util.c
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.h"

#include <stdio.h>

#include <stdlib.h>
#include <sys/select.h>
#include <arpa/inet.h>

char * atop(struct sockaddr_in * addr) {
	static char buff[32];
	char * ip = inet_ntoa(addr->sin_addr);
	unsigned short port = ntohs(addr->sin_port);
	sprintf(buff, "%s:%hu", ip, port);
	return buff;
}
