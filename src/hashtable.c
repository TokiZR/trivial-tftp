/*******************************************************************************
 * hashtable.c
 * TFTP - Trivial RFC 1350 implementation.
 * Copyright (C) 2015 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hashtable.h"

#include <string.h>
#include <malloc.h>
#include <stdint.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

/*******************************************************************************
 *
 *								Hash Table
 *
 ******************************************************************************/

typedef struct _HashtableNode HashtableNode;

struct _HashtableNode {
	void * data;
	HashtableNode * next;
	uint32_t key; /* The actual key of this node */
};

struct _Hashtable {
	size_t size;
	size_t load;
	HashtableNode * nodes[]; /* Nodes go Here */
};

static int hashtable_hash_key(int size, uint32_t key);

Hashtable * hashtable_new(size_t size) {
	Hashtable * tmp;
	int i;

	tmp = malloc(sizeof(Hashtable) + size * sizeof(HashtableNode *));
	if (!tmp) return NULL;

	tmp->load = 0;
	tmp->size = size;
	for (i = 0; i < size; i++) {
		tmp->nodes[i] = NULL;
	}

	return tmp;
}

void hashtable_delete(Hashtable * ht, void (*callback)(void * obj, void * data), void * cb_data) {
	int i;
	HashtableNode * node, *tmp;
	if (callback) {
		for (i = 0; i < ht->size; i++) {
			node = ht->nodes[i];
			while (node) {
				tmp = node;
				callback(node->data, cb_data);
				node = node->next;
				free(tmp);
			}
		}
	} else {
		for (i = 0; i < ht->size; i++) {
			node = ht->nodes[i];
			while (node) {
				tmp = node;
				node = node->next;
				free(tmp);
			}
		}
	}
	free(ht);
}

int hashtable_insert(Hashtable * ht, uint32_t key, void * data) {
	int hash;
	void * tmp;
	HashtableNode * node;

	hash = hashtable_hash_key(ht->size, key);

	node = ht->nodes[hash];
	if (node) {
		while (1) {
			if (key == node->key) {
				return -2;
			}
			if (node->next) {
				node = node->next;
			} else {
				break;
			}
		}

		tmp = malloc(sizeof(HashtableNode));
		if (!tmp) {
			return -1;
		}

		ht->load++;

		node->next = tmp;
		node = node->next;
		node->data = data;
		node->next = NULL;
		node->key = key;
	} else {
		tmp = malloc(sizeof(HashtableNode));
		if (!tmp) {
			return -1;
		}

		ht->load++;

		node = tmp;
		node->data = data;
		node->next = NULL;
		node->key = key;
		ht->nodes[hash] = node;
	}

	return 0;
}

void * hashtable_remove(Hashtable * ht, uint32_t key, int * found) {
	int hash;
	HashtableNode * node, *tmp;
	hash = hashtable_hash_key(ht->size, key);

	node = ht->nodes[hash];
	if (node && key == node->key) {
		tmp = node;
		ht->nodes[hash] = node->next;
		node = tmp->data; /* Dirty variable reusing */
		free(tmp);

		ht->load--;

		if (found) *found = 1;
		return node;
	} else {
		while (1) {
			if (key == node->next->key) {
				tmp = node->next;
				node->next = node->next->next;
				node = tmp->data; /* Dirty variable reusing */
				free(tmp);

				ht->load--;

				if (found) *found = 1;
				return node;
			}
			if (node->next) {
				node = node->next;
			} else {
				break;
			}
		}
	}

	if (found) *found = 0;
	return NULL;
}

int hashtable_has_key(Hashtable * ht, uint32_t key) {
	int hash;
	HashtableNode * node;
	hash = hashtable_hash_key(ht->size, key);

	node = ht->nodes[hash];
	while (node) {
		if (key == node->key) {
			return 1;
		}
		node = node->next;
	}
	return 0;
}

int hashtable_set(Hashtable * ht, uint32_t key, void * value) {
	int hash;
	void * tmp;
	HashtableNode * node;

	hash = hashtable_hash_key(ht->size, key);

	node = ht->nodes[hash];
	if (node) {
		while (1) {
			if (key == node->key) {
				node->data = value;
				return 0;
			}
			if (node->next) {
				node = node->next;
			} else {
				break;
			}
		}

		tmp = malloc(sizeof(HashtableNode));
		if (!tmp) {
			return -1;
		}

		ht->load++;

		node->next = tmp;
		node = node->next;
		node->data = value;
		node->next = NULL;
		node->key = key;
	} else {
		tmp = malloc(sizeof(HashtableNode));
		if (!tmp) {
			return -1;
		}

		ht->load++;

		node = tmp;
		node->data = value;
		node->next = NULL;
		node->key = key;
		ht->nodes[hash] = node;
	}

	return 0;
}

void * hashtable_get(Hashtable * ht, uint32_t key, int * found) {
	int hash;
	HashtableNode * node;
	hash = hashtable_hash_key(ht->size, key);

	node = ht->nodes[hash];
	while (node) {
		if (key == node->key) {
			if (found) *found = 1;
			return node->data;
		}
		node = node->next;
	}
	if (found) *found = 0;
	return NULL;
}

int hashtable_get_load(Hashtable * ht) {
	return ht->load;
}

int hashtable_get_size(Hashtable * ht) {
	return ht->size;
}

void hashtable_iterate(Hashtable * ht, void (*iterator)(uint32_t key, void * data, void * it_data), void * it_data) {
	size_t i;
	HashtableNode * n;


	for(i = 0; i < ht->size; i++) {
		n = ht->nodes[i];
		while(n) {
			iterator(n->key, n->data, it_data);
			n = n->next;
		}
	}
}

void hashtable_get_status_report(Hashtable * ht, int * size, int * load, int * avg_buck_size, int * max_buck_size,
		int * min_buck_size, int * bucket_sizes) {
	int i;
	HashtableNode * node;

	if (size) *size = ht->size;
	if (load) *load = ht->load;
	if (avg_buck_size) *avg_buck_size = ht->load / ht->size;

	if (bucket_sizes) {
		for (i = 0; i < ht->size; i++) {
			bucket_sizes[i] = 0;
			node = ht->nodes[i];
			while (node) {
				node = node->next;
				bucket_sizes[i]++;
			}
		}
		if (max_buck_size) {
			*max_buck_size = 0;
			for (i = 0; i < ht->size; i++) {
				if (*max_buck_size < bucket_sizes[i]) {
					*max_buck_size = bucket_sizes[i];
				}
			}
		}
		if (min_buck_size) {
			*min_buck_size = INT32_MAX;
			for (i = 0; i < ht->size; i++) {
				if (*min_buck_size > bucket_sizes[i]) {
					*min_buck_size = bucket_sizes[i];
				}
			}
		}
	} else {
		int min, max, len;
		min = INT32_MAX;
		max = 0;
		if (max_buck_size || min_buck_size) {
			for (i = 0; i < ht->size; i++) {
				len = 0;
				node = ht->nodes[i];
				while (node) {
					node = node->next;
					len++;
				}
				if (len > max) max = len;
				if (len < min) min = len;
			}
		}

		if (max_buck_size) *max_buck_size = max;
		if (min_buck_size) *min_buck_size = min;
	}
}

void hashtable_print_all(Hashtable * ht, FILE * stream) {
	int i;
	HashtableNode * node;
	fprintf(stream, "Hashtable (%zu/%zu)\n", ht->load, ht->size);

	for (i = 0; i < ht->size; i++) {
		node = ht->nodes[i];
		fprintf(stream, node ? "Node %d:\n\t" : "Node %d;", i);
		while (node) {
			fprintf(stream, "\"%u\": %p; ", node->key, node->data);
			node = node->next;
		}
		putc('\n', stream);
	}
}

/**
 * Good to have in case we want to make this cooler in the future.
 */
static int hashtable_hash_key(int size, uint32_t key) {
	return key % size;
}
