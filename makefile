export FILES = tftp.c tftp-server.c util.c hashtable.c
export LIBS = -lrt
.PHONY : all
all :
	@make -f svr.mk
	@make -f client.mk

.PHONY : clean
clean :
	@make -f svr.mk clean
	@make -f client.mk clean